<?php

/**
 * Class Theme
 *
 */

class Theme
{
    /**
     * Theme slug.
     *
     * @var string
     */
    const SLUG = 'kukutrip-theme';

    /**
     * Theme version.
     *
     * @var string
     */
    const VERSION = '1.0.0';

    /**
     * Theme constructor.
     */
  

    public function setup()
    {
    }

    /**
     * Register theme css files.
     */
    public function register_css_assets()
    {

        $suffix = SCRIPT_DEBUG ? '' : '.min';

        // Google Fonts

        // Vendor

        // Theme Base
        wp_enqueue_style(
            'kukutrip-theme-style',
            get_stylesheet_directory_uri() . '/style.css',
            [
                'kukutrip-theme-style',
            ],
            '1.0.0'
        );
    }

    /**
     * Register theme js files.
     */
    
}
// Init class.
new Theme();
