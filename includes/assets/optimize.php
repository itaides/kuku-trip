<?php
/**
 * Make this website faster
 * Copyright Itai ben zzev
 *
 */


 // Remove Global Styles and SVG Filters from WP 
function remove_global_styles_and_svg_filters() {
	remove_action( 'wp_enqueue_scripts', 'wp_enqueue_global_styles' );
	remove_action( 'wp_body_open', 'wp_global_styles_render_svg_filters' );
}
add_action('init', 'remove_global_styles_and_svg_filters');

// This snippet removes the Global Styles and SVG Filters that are mostly if not only used in Full Site Editing in WordPress 5.9.1+

// Remove Elementor Animation
function remove_animation() {
    wp_deregister_style( 'elementor-animations' );
    wp_dequeue_style( 'elementor-animations' );
}
add_action( 'wp_enqueue_scripts', 'remove_animation', 100 );

//Remove WP-embed/
function deregister_wpembed(){
	if ( ! is_admin()) {
		wp_deregister_script( 'wp-embed' );
	}
}
add_action( 'wp_enqueue_scripts', 'deregister_wpembed' );

//Remove Meta
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rest_output_link_wp_head');
remove_action( 'template_redirect', 'rest_output_link_header', 11 );
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('template_redirect', 'wp_shortlink_header', 11);
remove_action('wp_head', 'wp_generator');
add_filter('the_generator', '__return_false');
add_filter('feed_links_show_posts_feed', '__return_false');
remove_action('wp_head', 'feed_links_extra', 3);
add_filter('feed_links_show_comments_feed', '__return_false');


//remove
function dequeue_elementor_global__css() {
  wp_dequeue_style('elementor-global');
  wp_deregister_style('elementor-global');
}
add_action('wp_print_styles', 'dequeue_elementor_global__css', 9999);

//remmove images sizes
function add_image_insert_override( $sizes ){
    //unset( $sizes[ 'thumbnail' ]);
    unset( $sizes[ 'medium' ]);
    //unset( $sizes[ 'medium_large' ] );
    unset( $sizes[ 'large' ]);
    unset( $sizes[ 'full' ] );
    return $sizes;
}
add_filter( 'intermediate_image_sizes_advanced', 'add_image_insert_override' );