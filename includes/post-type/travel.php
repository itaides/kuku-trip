<?php
// Register Custom Post Type טיול
if ( ! function_exists('custom_travel') ) {

// Register Custom Post Type
function custom_travel() {

	$labels = array(
		'name'                  => _x( 'טיולים', 'Post Type General Name', 'travel_domain' ),
		'singular_name'         => _x( 'טיול', 'Post Type Singular Name', 'travel_domain' ),
		'menu_name'             => __( 'טיול', 'travel_domain' ),
		'name_admin_bar'        => __( 'טיולים', 'travel_domain' ),
		'archives'              => __( 'ארכיון טיולים', 'travel_domain' ),
		'attributes'            => __( 'תכונות טיול', 'travel_domain' ),
		'parent_item_colon'     => __( 'טיול אב', 'travel_domain' ),
		'all_items'             => __( 'כל הטיולים', 'travel_domain' ),
		'add_new_item'          => __( 'להוסיף טיול חדש', 'travel_domain' ),
		'add_new'               => __( 'להוסיף חדש', 'travel_domain' ),
		'new_item'              => __( 'טיול חדש', 'travel_domain' ),
		'edit_item'             => __( 'עריכת טיול', 'travel_domain' ),
		'update_item'           => __( 'עדכון טיול', 'travel_domain' ),
		'view_item'             => __( 'צפיה בטיול', 'travel_domain' ),
		'view_items'            => __( 'צפיה בטיולים', 'travel_domain' ),
		'search_items'          => __( 'חיפוש טיול', 'travel_domain' ),
		'not_found'             => __( 'לא נמצא', 'travel_domain' ),
		'not_found_in_trash'    => __( 'לא נמצא בפח הזבל', 'travel_domain' ),
		'featured_image'        => __( 'תמונה מוצגת בפוסט', 'travel_domain' ),
		'set_featured_image'    => __( 'בחירת תמונה', 'travel_domain' ),
		'remove_featured_image' => __( 'הסרת תמונה', 'travel_domain' ),
		'use_featured_image'    => __( 'שימוש כתמונה ראשית', 'travel_domain' ),
		'insert_into_item'      => __( 'להכניס לטיול', 'travel_domain' ),
		'uploaded_to_this_item' => __( 'העלאה לטיול', 'travel_domain' ),
		'items_list'            => __( 'רשימה', 'travel_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'travel_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'travel_domain' ),
	);
	$rewrite = array(
		'slug'                  => 'travel',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => false,
	);
	$args = array(
		'label'                 => __( 'טיול', 'travel_domain' ),
		'description'           => __( 'טיולים קוקו', 'travel_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'travel',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'travel', $args );

}
add_action( 'init', 'custom_travel', 0 );

}