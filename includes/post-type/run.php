<?php
// Register Custom Post Type ריצה
if ( ! function_exists('custom_run') ) {

// Register Custom Post Type
function custom_run() {

	$labels = array(
		'name'                  => _x( 'ריצות', 'Post Type General Name', 'travel_domain' ),
		'singular_name'         => _x( 'ריצה', 'Post Type Singular Name', 'travel_domain' ),
		'menu_name'             => __( 'ריצה', 'travel_domain' ),
		'name_admin_bar'        => __( 'ריצות', 'travel_domain' ),
		'archives'              => __( 'ארכיון ריצות', 'travel_domain' ),
		'attributes'            => __( 'תכונות ריצה', 'travel_domain' ),
		'parent_item_colon'     => __( 'ריצה אב', 'travel_domain' ),
		'all_items'             => __( 'כל הריצות', 'travel_domain' ),
		'add_new_item'          => __( 'להוסיף ריצה חדש', 'travel_domain' ),
		'add_new'               => __( 'להוסיף חדש', 'travel_domain' ),
		'new_item'              => __( 'ריצה חדש', 'travel_domain' ),
		'edit_item'             => __( 'עריכת ריצה', 'travel_domain' ),
		'update_item'           => __( 'עדכון ריצה', 'travel_domain' ),
		'view_item'             => __( 'צפיה בטיול', 'travel_domain' ),
		'view_items'            => __( 'צפיה בריצות', 'travel_domain' ),
		'search_items'          => __( 'חיפוש ריצה', 'travel_domain' ),
		'not_found'             => __( 'לא נמצא', 'travel_domain' ),
		'not_found_in_trash'    => __( 'לא נמצא בפח הזבל', 'travel_domain' ),
		'featured_image'        => __( 'תמונה מוצגת בפוסט', 'travel_domain' ),
		'set_featured_image'    => __( 'בחירת תמונה', 'travel_domain' ),
		'remove_featured_image' => __( 'הסרת תמונה', 'travel_domain' ),
		'use_featured_image'    => __( 'שימוש כתמונה ראשית', 'travel_domain' ),
		'insert_into_item'      => __( 'להכניס לטיול', 'travel_domain' ),
		'uploaded_to_this_item' => __( 'העלאה לטיול', 'travel_domain' ),
		'items_list'            => __( 'רשימה', 'travel_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'travel_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'travel_domain' ),
	);
	$rewrite = array(
		'slug'                  => 'run',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => false,
	);
	$args = array(
		'label'                 => __( 'ריצה', 'travel_domain' ),
		'description'           => __( 'ריצות קוקו', 'travel_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats' ),
		'taxonomies'            => array( 'category', 'post_tag','location' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'run',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'run', $args );

}
add_action( 'init', 'custom_run', 0 );

}