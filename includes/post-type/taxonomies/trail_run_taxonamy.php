<?php

function cptui_register_my_taxes_shvilim() {

	/**
	 * Taxonomy: תוואי הריצה.
	 */

	$labels = [
		"name" => esc_html__( "תוואי הריצה", "custom-post-type-ui" ),
		"singular_name" => esc_html__( "תוואי לריצה", "custom-post-type-ui" ),
		"menu_name" => esc_html__( "תוואי לריצה", "custom-post-type-ui" ),
		"all_items" => esc_html__( "כל התוואי", "custom-post-type-ui" ),
		"edit_item" => esc_html__( "עריכת תוואי", "custom-post-type-ui" ),
		"view_item" => esc_html__( "הצגת תוואי", "custom-post-type-ui" ),
		"update_item" => esc_html__( "עדכון תואי", "custom-post-type-ui" ),
		"add_new_item" => esc_html__( "הוספת תוואי", "custom-post-type-ui" ),
		"new_item_name" => esc_html__( "הוספת שם תוואי חדש", "custom-post-type-ui" ),
		"parent_item" => esc_html__( "אב - תוואי", "custom-post-type-ui" ),
		"search_items" => esc_html__( "חיפוש תוואי", "custom-post-type-ui" ),
		"popular_items" => esc_html__( "תוואי פופלרי", "custom-post-type-ui" ),
	];

	
	$args = [
		"label" => esc_html__( "תוואי הריצה", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'shvilim', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => true,
		"rest_base" => "shvilim",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"rest_namespace" => "wp/v2",
		"show_in_quick_edit" => true,
		"sort" => true,
		"show_in_graphql" => false,
	];
	register_taxonomy( "shvilim", [ "post", "run" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes_shvilim' );