<?php

function cptui_register_my_taxes_location() {

/**
 * Taxonomy: location.
 */

$labels = [
    "name" => esc_html__( "location", "custom-post-type-ui" ),
    "singular_name" => esc_html__( "loci", "custom-post-type-ui" ),
    "menu_name" => esc_html__( "מיקום", "custom-post-type-ui" ),
    "all_items" => esc_html__( "All location", "custom-post-type-ui" ),
    "edit_item" => esc_html__( "Edit loci", "custom-post-type-ui" ),
    "view_item" => esc_html__( "View loci", "custom-post-type-ui" ),
    "update_item" => esc_html__( "Update loci name", "custom-post-type-ui" ),
    "add_new_item" => esc_html__( "Add new loci", "custom-post-type-ui" ),
    "new_item_name" => esc_html__( "New loci name", "custom-post-type-ui" ),
    "parent_item" => esc_html__( "Parent loci", "custom-post-type-ui" ),
    "parent_item_colon" => esc_html__( "Parent loci:", "custom-post-type-ui" ),
    "search_items" => esc_html__( "Search location", "custom-post-type-ui" ),
    "popular_items" => esc_html__( "Popular location", "custom-post-type-ui" ),
    "separate_items_with_commas" => esc_html__( "Separate location with commas", "custom-post-type-ui" ),
    "add_or_remove_items" => esc_html__( "Add or remove location", "custom-post-type-ui" ),
    "choose_from_most_used" => esc_html__( "Choose from the most used location", "custom-post-type-ui" ),
    "not_found" => esc_html__( "No location found", "custom-post-type-ui" ),
    "no_terms" => esc_html__( "No location", "custom-post-type-ui" ),
    "items_list_navigation" => esc_html__( "location list navigation", "custom-post-type-ui" ),
    "items_list" => esc_html__( "location list", "custom-post-type-ui" ),
];


$args = [
    "label" => esc_html__( "location", "custom-post-type-ui" ),
    "labels" => $labels,
    "public" => true,
    "publicly_queryable" => true,
    "hierarchical" => false,
    "show_ui" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "query_var" => true,
    "rewrite" => [ 'slug' => 'location', 'with_front' => true, ],
    "show_admin_column" => true,
    "show_in_rest" => true,
    "show_tagcloud" => true,
    "rest_base" => "location",
    "rest_controller_class" => "WP_REST_Terms_Controller",
    "rest_namespace" => "wp/v2",
    "show_in_quick_edit" => true,
    "sort" => false,
    "show_in_graphql" => false,
];
register_taxonomy( "location", [ "post","run","travel" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes_location' );