<?php
function taxonamy_register_tiul() {

	/**
	 * Taxonomy: קושי הטיול.
	 */

	$labels = [
		"name" => esc_html__( "קושי הטיול", "custom-post-type-ui" ),
		"singular_name" => esc_html__( "דרגת הקושי טיול", "custom-post-type-ui" ),
	];

	
	$args = [
		"label" => esc_html__( "קושי הטיול", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'tiul', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => true,
		"rest_base" => "tiul",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"rest_namespace" => "wp/v2",
		"show_in_quick_edit" => false,
		"sort" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "tiul", [ "post","travel" ], $args );
}
add_action( 'init', 'taxonamy_register_tiul' );