<?php

/**
 * Theme functions and definitions
 */

/**
 * Load child theme css and optional scripts
 *
 * @return void
 */

// Theme Class.
require_once 'includes/class-theme.php';

add_action( 'wp_enqueue_scripts', function () {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
});

// Hooks

// Post Types		
require_once 'includes/post-type/run.php';
require_once 'includes/post-type/travel.php';
require_once 'includes/post-type/taxonomies/location_run_taxonamy.php';
require_once 'includes/post-type/taxonomies/location_taxonamy.php';
require_once 'includes/post-type/taxonomies/travel_level_taxonamy.php';
require_once 'includes/post-type/taxonomies/trail_run_taxonamy.php';

// Assets
require_once 'includes/assets/optimize.php';
// Shortcodes.

// Macros (Jet Engine).